var express = require('express'),
	router = express.Router(),
	ActiveDirectory = require('activedirectory');

var admin_username = 'ADAMAdmin',
	admin_password = 'ADAMAdmin';
	
var createDN = function( username ){
	var baseDN = [];
	if( username ) {
		baseDN.push( 'CN=' + username );
	};
	baseDN.push( 'OU=Users' );
	baseDN.push( 'O=TestDirectory' );
	
	return baseDN.join(',');
};

var createAd = function( ){
	var configs = { 
		username : createDN( admin_username ),
		password : admin_password,
		url: 'ldap://localhost:50000',
		baseDN: createDN(),
		attributes: {
			user: [ 'myCustomAttribute', 'mail', 'userPrinicipalName' ],
			group: [ 'anotherCustomAttribute', 'objectCategory' ]
		}
	};
	var ad = new ActiveDirectory( configs );
	
	return ad;
};

router.get('/', function(req, res) {
	res.json({status:false});
});

router.post('/auth', function(req, res) {
	var username = new String( req.body.username ).trim();
	var password = new String( req.body.password ).trim();
	
	createAd().authenticate( createDN( username ), password, function( err, auth ) {
		if (err) {
			res.json( err );
		}else{
			res.json({ status : auth, token : 'token-set-disini-or-etc', message : ( auth ? 'Authentication success' : 'Authentication failed!' )});
		};
	});
});

router.get('/findusers', function(req, res) {
	var username = new String( req.param('username') ).trim();
	var query 	 = 'cn=*'+username+'*';
	
	createAd().findUsers( query, true, function( err, users ) {
		if (err) {
			res.json( err );
		}else{
			res.json({ status : users ? true : false, users : users });
		};
	});
});

module.exports = router;
